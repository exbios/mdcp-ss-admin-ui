FROM keymetrics/pm2:10-alpine

#install aws-cli, needed by startup.sh
RUN apk update && apk add --no-cache python2 py-pip && rm -rf /var/cache/apk/*
RUN pip --disable-pip-version-check install awscli 
WORKDIR /app
COPY . /app

COPY package*.json ./

RUN npm cache verify
RUN rm -rf node_modules
RUN npm install --unsafe-perm node-sass

RUN npm run build

RUN chmod +x /app/start.sh
ENTRYPOINT ["/app/start.sh"]

ENV HOST 0.0.0.0
CMD ["pm2-runtime", "process.yml"]

EXPOSE 3000