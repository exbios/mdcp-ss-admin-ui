#!/bin/sh
set -e

# This will exec the CMD from the Dockerfile, i.e. "node /some/file.js"
exec "$@"

