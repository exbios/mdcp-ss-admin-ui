# mdcp-ss-admin-ui

> Admin UI for SS

## Build Setup

``` bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# test components with Jest
$ npm test

# build for production and launch server
$ npm run build
$ npm run start

### Code Formatting and Linting

#### ESLint

[ESLint](https://eslint.org/) is a pluggable linting utility for JavaScript. It
is used in this codebase specifically to enforce JavaScript formatting rules,
which are defined in the `.eslintrc` file.

ESLint is integrated to the build step, to manually run it:

$ npm run lint

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
