#!/bin/bash
export IS_REPO_CREATED=`aws ecr describe-repositories --region $REGION | jq --arg repo_name "$ECR_REPO_NAME" '.repositories[] | select(.repositoryName==$repo_name) | .repositoryName'`

  if [ -z $IS_REPO_CREATED ]; then
    export CREATE_ECR_BOOL='Yes'
    echo "The ECR repository: $IS_REPO_CREATED is not created with bool: $CREATE_ECR_BOOL"
  else
    export CREATE_ECR_BOOL='No'
    echo "The ECR repository: $IS_REPO_CREATED is already created with bool: $CREATE_ECR_BOOL"
  fi

function create_env_file() {
  export ENV_NAME=$1

  if [ -z $ENV_NAME ]; then
    echo "USE: create_env_file ENV_NAME"
    exit 1
  else
    echo -e "\x1b[32mCreating the enviroment configuration file for the Elastic Beanstalk application\x1b[0m"
    mkdir -p ./vars/environment/$ENV_NAME
    cat ./vars/environment/prod/vars.yml | sed '3s/New/Existing/g' | sed '4s/prod/'"$ENV_NAME"'/g' | sed '5s/Yes/'"$CREATE_ECR_BOOL"'/g' | sed '10s/production/development/g' > ./vars/environment/$ENV_NAME/vars.yml
    echo -e "\x1b[31mCreated the configuration file into the ./vars/environment/$ENV_NAME/vars.yml:\x1b[0m"
    echo -e "\x1b[32m$(cat ./vars/environment/$ENV_NAME/vars.yml)\x1b[0m\n"
  fi
}

if ! $(command -v yamllint >/dev/null 2>&1); then
    pip install yamllint
fi

export branch_env=$(echo $CIRCLE_BRANCH | cut -d'-' -f 1)

if [ $branch_env = "mdcpss" ]; then
  export env="${CIRCLE_BRANCH//-}"
  create_env_file $env
elif [ $branch_env = "master" ]; then
  export env="prod"
elif [ $branch_env = "dev" ]; then
  export env="development"
  create_env_file $env
else
  echo -e "\x1b[31mBranch $branch_env not recognized, only enable, dev, master, mdcpss-XYZ\x1b[0m"
  exit 1
fi

echo -e "\x1b[32m$(cat ./vars/environment/$env/vars.yml)\x1b[0m\n"