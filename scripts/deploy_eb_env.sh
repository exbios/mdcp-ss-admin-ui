#!/bin/bash -e

REGION=$AWS_DEFAULT_REGION
STACK_NAME=StackSet-$CIRCLE_PROJECT_REPONAME
TEMPLATE=cfn/template.yml
CUSTOMER=$CUSTOMER_NAME
PURPOSE=hosting

# Creating the environments for feature branch and validate if the ECR repo is already created
.  $HOME/project/scripts/deploy_eb_env_tools.sh

# set -exo pipefail
cfn-lint --ignore-checks W1020 W2001 W8001 -t $TEMPLATE
yamllint -d "{extends: relaxed, rules: {line-length: {max: 180}}}" $TEMPLATE

echo -e "Start the cloudformation deployment - \x1b[31m REGION: $REGION, ENV: $env, STACK_NAME: $STACK_NAME and TEMPLATE: $TEMPLATE\x1b[0m\n"
$DOCKER_CMD_PREFIX digops-stacks change-set $STACK_NAME $TEMPLATE \
      --environment $env \
      --region $REGION \
      --customer $CUSTOMER \
      --purpose $PURPOSE
$DOCKER_CMD_PREFIX digops-stacks execute $STACK_NAME \
      --environment $env \
      --region $REGION

exit 0
