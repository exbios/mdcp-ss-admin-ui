#!/bin/bash
################################################################################
#
#
# enable
set -e

# enable debug
#set -x

################################################################################
# Parameters
CF_TEMPLATE_FILE="./deployment/cf-beanstalk.yml"
CF_CONFIG_FILE="./deployment/parameters.json"
REPO_NAME=`basename $(git remote show -n origin | grep Push | cut -d: -f2- | cut -d\. -f2)`
PROGNAME=$(basename $0)

################################################################################
# Validate arguments
if [ $# -lt 1 ]
then
  echo "Usage: ${PROGNAME} init|destroy"
  exit 1
fi

ARG=$1
envDeployName=$2
domainName=$3

################################################################################
# Functions
function file_exist() {
  local name="$1"
  echo "Checando esta wea $name"
  if [ -f $CF_TEMPLATE ] ; then
    echo true
  else
    echo false
  fi
}

function get_param() {
  local name="$1"
  cat $CF_CONFIG_FILE | jq -r --arg name $name '.[] | select(.ParameterKey == $name) | .ParameterValue'
}

function bucket_exist() {
    local bucket="$1"
    local region="$2"
    if aws s3 ls s3://$bucket --region $region --profile myaws 2>&1 | grep -q "NoSuchBucket"; then
      echo false
    else
      echo true
    fi
}

function cf_stack_exist() {
    local name="$1"
    local region="$2"
    local envName="$3"
    if aws cloudformation describe-stacks \
      --stack-name ${name}-${envName} \
      --region ${region} \
      --profile myaws 2>&1 | grep -q "ValidationError"; then
      echo false
    else
      echo true
    fi
}

function domain_name_enable() {
  local domainName="$1"
  if aws elasticbeanstalk check-dns-availability --cname-prefix $domainName --profile myaws --region us-east-1 2>&1 | jq '.Available'; then
    echo true
  else
    echo false
  fi
}

function template_name_enable() {
  local templateName="$1"
  if aws elasticbeanstalk describe-applications --profile myaws --region us-east-1 | jq -c '.Applications[] | .ConfigurationTemplates ' | grep $templateName; then
    echo true
  else 
    echo false
  fi
}

function verify_app_version() {
  local appName="$1"
  local appVersion="$2"
  aws elasticbeanstalk describe-application-versions \
    --application-name $appName  --version-label $appVersion \
    --profile myaws --region us-east-1 | jq -r ".ApplicationVersions[] | .VersionLabel"
}

function get_app_version() {
  local appName="$1"
  aws elasticbeanstalk describe-applications \
    --profile myaws \
    --region us-east-1 2> /dev/null | jq -r " .Applications[] | select(.ApplicationName == \"$appName\" ) | .Versions[0]"
}

function create_new_template() {
  local templateName="$1"
  local appName="$2"
  aws elasticbeanstalk create-configuration-template \
    --application-name $appName \
    --template-name $templateName \
    --solution-stack-name "64bit Amazon Linux 2018.03 v4.12.0 running Node.js" \
    --profile myaws \
    --region us-east-1 2> /dev/null
}

function create_new_env() {
  local domainName="$1"
  local templateName="$2"
  local versionApp="$3"
  local envName="$4"
  aws elasticbeanstalk create-environment --cname-prefix $domainName \
    --application-name mdcp-ss-admin-ui --template-name $templateName \
    --version-label $versionApp \
    --environment-name $envName \
    --option-settings file://deployment/createEnvParameters.json \
    --profile myaws --region us-east-1
}

function cf_create_stack() {
  local name="$1"
  local template="$2"
  local config_file="$3"
  local region="$4"
  local envName="$5"
  aws cloudformation create-stack \
    --stack-name "$name-$envName" \
    --template-body file://$template \
    --parameters file://$config_file \
    --capabilities CAPABILITY_IAM \
    --on-failure DELETE \
    --region $region \
    --profile myaws
    
}

function cf_delete_stack() {
  local name="$1"
  local region="$2"
  local envName="$3"
  aws cloudformation delete-stack \
    --stack-name $name-$envName \
    --region $region \
    --profile myaws
    
}

function create_bucket() {
  local name="$1"
  local region="$2"
  aws s3api create-bucket \
    --bucket $name \
    --region $region \
    --profile myaws
    
}

function delete_bucket() {
  local name="$1"
  local region="$2"
  aws s3 rm s3://$name --recursive --region $region --profile myaws
}

function wait_until_cf_stack_creation() {
  local name="$1"
  local region="$2"
  local envName="$3"
  echo "Waiting until Cloudformation stack \"${name}\" is created, takes a long time!"
  aws cloudformation wait stack-create-complete \
    --stack-name $name-$envName \
    --region $region \
    --profile myaws
    
  echo "Cloudformation stack \"${name}\" was created"
}

function upload_artifact() {
  local bucket="$1"
  local key="$2"
  local region="$3"
  # zip -r $key ./dist | aws s3 cp - s3://$bucket/$key --region $region --grants full=id=6dc1576fd6888e1327717ce810cdae909bc27c155fa5a1b2b8df8a2fc5532b2a --profile myaws
  git archive --format zip HEAD | aws s3 cp - s3://$bucket/$key  --region $region --profile myaws
}

function eb_init() {
  local region="$1"
  eb init --platform node.js --region $region --profile myaws
}

function eb_use() {
  local env="$1"
  eb use $env --profile myaws --region us-east-1
}



################################################################################

CF_STACK_NAME=$(get_param ApplicationName)
STACK_ENVIRONMENT=$(get_param EnvironmentName)
BUCKET=$(get_param ApplicationS3Bucket)
STACK_ENVIRONMENT=$(get_param EnvironmentName)
KEY=$(get_param ApplicationS3Artifact)
REGION=$(get_param AWSRegion)
BUCKET_EXIST=$(bucket_exist ${BUCKET} ${REGION})
CF_STACK_EXIST=$(cf_stack_exist ${CF_STACK_NAME} ${REGION} ${STACK_ENVIRONMENT})
CF_TEMPLATE_FILE_EXIST="$(file_exist $CF_TEMPLATE_FILE)"
CF_CONFIG_FILE_EXIST=$(file_exist $CF_CONFIG_FILE)
APP_VERSION=$(get_app_version "mdcp-ss-admin-ui")
IF_ENABLE_APP_VERSION=$(verify_app_version $CF_STACK_NAME $APP_VERSION)
IS_DOMAIN_NAME_ENABLE=$(domain_name_enable $domainName)

echo -e "\x1b[31m"
echo "CF_STACK_NAME-> $CF_STACK_NAME \n"
echo "BUCKET-> $BUCKET \n"
echo "KEY $KEY\n"
echo "REGION $REGION\n"
echo "BUCKET_EXIST $BUCKET_EXIST\n"
echo "CF_STACK_EXIST $CF_STACK_EXIST\n"
echo "CF_TEMPLATE_FILE_EXIST $CF_TEMPLATE_FILE_EXIST\n"
echo "CF_CONFIG_FILE_EXIST $CF_CONFIG_FILE_EXIST\n"
echo "STACK_ENVIRONMENT $STACK_ENVIRONMENT\n"
echo "APP_VERSION $APP_VERSION \n"
echo "IS_DOMAIN_NAME_ENABLE $IS_DOMAIN_NAME_ENABLE \n"
echo -e "\x1b[0m"

if [[ "${CF_TEMPLATE_FILE_EXIST}" == false ]]; then
  echo "${CF_TEMPLATE_FILE} is necessary"
fi

if [[ "${CF_CONFIG_FILE_EXIST}" == false ]]; then
  echo "${CF_CONFIG_FILE} is necessary"
fi

#
if [[ "${ARG}" == "init" ]]; then

  # Create a BUCKET if not exist
  if [[ "${BUCKET_EXIST}" == false ]]; then
    create_bucket $BUCKET $REGION
  fi

  # upload our artifact to S3
  echo "Uploading $KEY into $BUCKET and region $REGION"
  upload_artifact $BUCKET $KEY $REGION

  if [[ "${CF_STACK_EXIST}" == false ]]; then
    cf_create_stack $CF_STACK_NAME $CF_TEMPLATE_FILE $CF_CONFIG_FILE $REGION $STACK_ENVIRONMENT
    wait_until_cf_stack_creation $CF_STACK_NAME $REGION $STACK_ENVIRONMENT
    # eb_init $REGION
    eb_use $STACK_ENVIRONMENT
    # cp deployment/cloudwatch.config .ebextensions/
    eb deploy $envDeployName --profile myaws --region us-east-1
  fi

elif [[ "${ARG}" == "destroy" ]]; then

  if [[ "${CF_STACK_EXIST}" == true ]]; then
    cf_delete_stack $CF_STACK_NAME $REGION $STACK_ENVIRONMENT
  fi

  # Create a BUCKET if not exist
  if [[ "${BUCKET_EXIST}" == true ]]; then
    delete_bucket $BUCKET $REGION
  fi

elif [[ "${ARG}" == "createEnv" ]]; then

 echo  "Current version $APP_VERSION"
 # Verify if the appVersion is correct
  if [[ "${IF_ENABLE_APP_VERSION}" == "$APP_VERSION" ]]; then
     echo "The appVersion is enable and correct"
     if [[ ${IS_DOMAIN_NAME_ENABLE} ]]; then
       echo "Domain Name enable - Creating a new env"
       if template_name_enable $envDeployName; then
         create_new_env $domainName $envDeployName $APP_VERSION $envDeployName
       else
         create_new_template $envDeployName $CF_STACK_NAME
         create_new_env $domainName $envDeployName $APP_VERSION $envDeployName
      fi
     fi
  fi

else
  echo "Invalid argument"
fi


